import { Component } from '@angular/core';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent {
    public tamano: number;
    public dificultad: number;
    public mostrar: boolean;

    constructor() {
    }

    jugar() {
        this.mostrar = true;
    }
    reniciar(){
        this.mostrar = false;

    }
}
