import { Component, Input, OnInit } from '@angular/core';
import { Ladrillo } from '../entidades/ladrillo';

@Component({
  selector: 'app-grilla',
  templateUrl: './grilla.component.html',
  styleUrls: ['./grilla.component.css']
})
export class GrillaComponent implements OnInit  {
    public tablero: any;
    public _dificultad;
    @Input() dificultad : string;
    @Input() tamano: string;
    public numFilas: number;
    public numColumnas: number;
    public selec = false;
    public ladrillo: Ladrillo;
    public modal: boolean;

    ngOnInit(): void {
        this.confDificultad();
        this.confTamaño();
        
    }
  constructor() {
  
  }
  crearMatris(filas: number, columnas: number) {
    this.numColumnas = columnas - 1;
    this.numFilas = filas - 1;
    this.tablero = new Array(filas);
    for (let x = 0; x < this.tablero.length; x++) {
        this.tablero[x] = new Array(columnas);
        for (let y = 0; y < this.tablero[x].length; y++) {
            this.tablero[x][y] = new Ladrillo();
            this.tablero[x][y].x = x;
            this.tablero[x][y].y = y;
            this.tablero[x][y].bum= false;
            this.tablero[x][y].marca = 0;
        }
    }
    this.minar();
    console.log(this.tablero);

  }
  minar() {
    for (let x = 0; x < this.tablero.length; x++) {
        for (let y = 0; y < this.tablero[x].length; y++) {
            this.tablero[x][y].visible = false;
            if (Math.random() < this._dificultad) {
                this.tablero[x][y].valor = -1;
                // arriba
                if (x > 0 ) {
                    if (this.tablero[x - 1][y].valor !== -1) {
                        this.tablero[x - 1][y].valor = this.tablero[x - 1][y].valor ? this.tablero[x - 1][y].valor + 1 : 1;
                    }
                }
                // abajo
                if (x < this.numFilas) {
                    if ( this.tablero[x + 1][y].valor !== -1) {
                    this.tablero[x + 1][y].valor = this.tablero[x + 1][y].valor ? this.tablero[x + 1][y].valor + 1 : 1;
                    }
                }
                // isquierda
                if (y > 0) {
                    if (this.tablero[x][y - 1].valor !== -1) {
                    this.tablero[x][y - 1].valor = this.tablero[x][y - 1].valor ? this.tablero[x][y - 1].valor + 1 : 1;
                    }
                }
                // derecha
                if (y < this.numColumnas) {
                    if (this.tablero[x][y + 1].valor !== -1) {
                    this.tablero[x][y + 1].valor = this.tablero[x][y + 1].valor ? this.tablero[x][y + 1].valor + 1 : 1;
                    }
                }
                // arriba isquierda
                if (x > 0 && y > 0) {
                    if (this.tablero[x - 1][y - 1].valor !== -1) {
                    this.tablero[x - 1][y - 1].valor = this.tablero[x - 1][y - 1].valor ? this.tablero[x - 1][y - 1].valor + 1 : 1;
                    }
                }
                // arriba derecha
                if (x > 0 && y < this.numColumnas) {
                    if ( this.tablero[x - 1][y + 1].valor !== -1) {
                    this.tablero[x - 1][y + 1].valor = this.tablero[x - 1][y + 1].valor ? this.tablero[x - 1][y + 1].valor + 1 : 1;
                    }
                }
                // abajo isquierda
                if (x < this.numFilas && y > 0) {
                    if (this.tablero[x + 1][y - 1].valor !== -1) {
                    this.tablero[x + 1][y - 1].valor = this.tablero[x + 1][y - 1].valor ? this.tablero[x + 1][y - 1].valor + 1 : 1;
                    }
                }
                // abajo derecha
                if (x < this.numFilas && y < this.numColumnas) {
                    if (this.tablero[x + 1][y + 1].valor !== -1) {
                    this.tablero[x + 1][y + 1].valor = this.tablero[x + 1][y + 1].valor ? this.tablero[x + 1][y + 1].valor + 1 : 1;
                    }
                }
            }
        }
    }
  }
  clickTablero(ladrillo: Ladrillo) {
   if (ladrillo.valor === -1) {
    ladrillo.bum= true;
       // cartel game over¨
       this.tablero.forEach(x => x.forEach(y => y.visible = true));
       this.modal = true;
       return;
   }
   this.mostrarContenido(ladrillo);
  }
  mostrarContenido(ladrillo: Ladrillo) {
      if(ladrillo.visible === true){
          return;
      }
    if (ladrillo.valor !== -1) {
        ladrillo.visible = true;
    }
    if (!ladrillo.valor) {
     this.propagacion(ladrillo);
   }
  }
  propagacion(ladrillo: Ladrillo) {
      // arriba
      if (ladrillo.x > 0) {
        this.mostrarContenido(this.tablero[ladrillo.x - 1 ][ladrillo.y]);
      }
      // abajo
      if (ladrillo.x < this.numFilas) {
        this.mostrarContenido(this.tablero[ladrillo.x + 1 ][ladrillo.y]);
      }
      // isquierda
      if (ladrillo.y > 0) {
         this.mostrarContenido(this.tablero[ladrillo.x][ladrillo.y - 1]);
      }
      // derecha
      if (ladrillo.y < this.numColumnas) {
         this.mostrarContenido(this.tablero[ladrillo.x][ladrillo.y + 1]);

      }
      // arriba isquierda
      if (ladrillo.x > 0 && ladrillo.y > 0) {
          this.mostrarContenido(this.tablero[ladrillo.x - 1][ladrillo.y - 1]);
      }
      // arriba derecha
      if (ladrillo.x > 0 && ladrillo.y < this.numColumnas) {
          this.mostrarContenido(this.tablero[ladrillo.x - 1][ladrillo.y + 1]);
      }
      // abajo isquierda
      if (ladrillo.x < this.numFilas && ladrillo.y > 0) {
          this.mostrarContenido(this.tablero[ladrillo.x + 1][ladrillo.y - 1]);
      }
      // abajo derecha
      if (ladrillo.x < this.numFilas && ladrillo.y < this.numColumnas) {
          this.mostrarContenido(this.tablero[ladrillo.x + 1][ladrillo.y + 1]);
      }
  }
  marcar(ladrillo: Ladrillo) {
    if(ladrillo.marca !==2){
        ladrillo.marca ++;
    }else{
        ladrillo.marca = 0;

    }
    return false;
  }
  confDificultad(){
      switch (this.dificultad) {
          case '1':
              this._dificultad= 0.20
              break;
              case '2':
              this._dificultad= 0.30
              break; 
              case '3':
              this._dificultad= 0.40
              break;
          default:
              break;
      }
}
confTamaño(){
    switch (this.tamano) {
        case '1':
            this.crearMatris(5, 5);
            break;
            case '2':
            this.crearMatris(10, 10);
            break; 
            case '3':
            this.crearMatris(20,20);
            break;
        default:
            break;
    }
}
}

