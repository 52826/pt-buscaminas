
export class Ladrillo {
    visible: boolean;
    valor: number;
    x: number;
    y: number;
    bum:boolean;
    marca:number;

    constructor() {
        this.visible = undefined;
        this.valor = undefined;
        this.x = undefined;
        this.y = undefined;
        this.bum =undefined;
        this.marca =undefined;
    }
}